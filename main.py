"""
The MIT License (MIT)
Copyright © 2022 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/micropython-esp32-walkie-talkie
"""
from test.verify_board import VerifyBoard


if __name__ == '__main__':
	board = VerifyBoard()
