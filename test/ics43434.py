"""
The MIT License (MIT)
Copyright © 2022 Walkline Wang (https://walkline.wang)
Gitee: https://gitee.com/walkline/micropython-esp32-walkie-talkie
"""

'''
| ICS-43434 | ESP32 |
|-------------------|
|    WS <===> IO25  |
|    SD <===> IO26  |
|   SCK <===> IO27  |
|    LR <===> GND   |
|   GND <===> GND   |
|   VDD <===> 3V3   |
'''
from machine import Pin, I2S

BUFF_SIZE = 20000
WAVE_SAMPLES_MV = memoryview(bytearray(BUFF_SIZE))

BITS = 16
FORMAT = I2S.MONO
RATE = 16000


class Pins(object):
	BUTTON = 0

	SD = Pin(26)
	SCK = Pin(27)
	WS = Pin(25)


class ICS43434(object):
	def __init__(self):
		self.__audio = I2S(
			0,
			sck=Pins.SCK, 
			ws=Pins.WS,
			sd=Pins.SD,
			mode=I2S.RX,
			bits=BITS,
			format=FORMAT,
			rate=RATE,
			ibuf=BUFF_SIZE
		)

	def read(self):
		print('read: ', self.__audio.readinto(WAVE_SAMPLES_MV))
		# I2S.shift(buf=WAVE_SAMPLES_MV, bits=16, shift=0)

		return WAVE_SAMPLES_MV


def main():
	mic = ICS43434()

	for _ in range(20):
		mic.read()


if __name__ == '__main__':
	main()
