### GPIO

```cpp
/* Set 1 to allocate rx & tx channels in duplex mode on a same I2S controller, they will share the BCLK and WS signal
 * Set 0 to allocate rx & tx channels in simplex mode, these two channels will be totally separated,
 * Specifically, due to the hardware limitation, the simplex rx & tx channels can't be registered on the same controllers on ESP32 and ESP32-S2,
 * and ESP32-S2 has only one I2S controller, so it can't allocate two simplex channels */
#define EXAMPLE_I2S_DUPLEX_MODE         CONFIG_USE_DUPLEX

#if CONFIG_IDF_TARGET_ESP32
    #define EXAMPLE_STD_BCLK_IO1        GPIO_NUM_4      // I2S bit clock io number
    #define EXAMPLE_STD_WS_IO1          GPIO_NUM_5      // I2S word select io number
    #define EXAMPLE_STD_DOUT_IO1        GPIO_NUM_18     // I2S data out io number
    #define EXAMPLE_STD_DIN_IO1         GPIO_NUM_19     // I2S data in io number
    #if !EXAMPLE_I2S_DUPLEX_MODE
        #define EXAMPLE_STD_BCLK_IO2    GPIO_NUM_22     // I2S bit clock io number
        #define EXAMPLE_STD_WS_IO2      GPIO_NUM_23     // I2S word select io number
        #define EXAMPLE_STD_DOUT_IO2    GPIO_NUM_25     // I2S data out io number
        #define EXAMPLE_STD_DIN_IO2     GPIO_NUM_26     // I2S data in io number
    #endif
#else
    #define EXAMPLE_STD_BCLK_IO1        GPIO_NUM_2      // I2S bit clock io number
    #define EXAMPLE_STD_WS_IO1          GPIO_NUM_3      // I2S word select io number
    #define EXAMPLE_STD_DOUT_IO1        GPIO_NUM_4      // I2S data out io number
    #define EXAMPLE_STD_DIN_IO1         GPIO_NUM_5      // I2S data in io number
    #if !EXAMPLE_I2S_DUPLEX_MODE
        #define EXAMPLE_STD_BCLK_IO2    GPIO_NUM_6      // I2S bit clock io number
        #define EXAMPLE_STD_WS_IO2      GPIO_NUM_7      // I2S word select io number
        #define EXAMPLE_STD_DOUT_IO2    GPIO_NUM_8      // I2S data out io number
        #define EXAMPLE_STD_DIN_IO2     GPIO_NUM_9      // I2S data in io number
    #endif
#endif
```

```python
from machine import I2S
from machine import Pin

# ESP32
sck_pin = Pin(14)   # Serial clock output
ws_pin = Pin(13)    # Word clock output
sd_pin = Pin(12)    # Serial data output

or

# PyBoards
sck_pin = Pin("Y6")   # Serial clock output
ws_pin = Pin("Y5")    # Word clock output
sd_pin = Pin("Y8")    # Serial data output

audio_out = I2S(2,
                sck=sck_pin, ws=ws_pin, sd=sd_pin,
                mode=I2S.TX,
                bits=16,
                format=I2S.MONO,
                rate=44100,
                ibuf=20000)

audio_in = I2S(2,
               sck=sck_pin, ws=ws_pin, sd=sd_pin,
               mode=I2S.RX,
               bits=32,
               format=I2S.STEREO,
               rate=22050,
               ibuf=20000)
```

